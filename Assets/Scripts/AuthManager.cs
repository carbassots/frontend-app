using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using Proyecto26;
using SimpleHTTP;

public class AuthManager : MonoBehaviour
{
    public string username;
    public string password;
    public string token;
    public string firstScreen;
    public string role;
    public bool isUserLogged;
    public float caragols;
    public LoginJson loginInfo;

    public static AuthManager reference;

    // Start is called before the first frame update
    void Start()
    {
        caragols = 10;
        if(reference == null)
        {
            reference = this;
        } else
        {
            return;
        }

        GameObject.Find("Canvas").GetComponent<PanelManager>().Show(firstScreen);

        //TestNet();
        //TestNotification();
        //TestNet2();
    }

    void TestNet2()
    {
        StartCoroutine(SetToken());
    }

    void TestNet()
    {
        //StartCoroutine(GetRequest("https://jsonplaceholder.typicode.com/posts"));

        //RestClient.Get("https://jsonplaceholder.typicode.com/posts").Then(res => {
        //    Debug.Log(res.Text);
        //});
        RequestHelper currentRequest;

        currentRequest = new RequestHelper
        {
            Uri = "http://15.188.80.3:8000" + "/api/users/login",
            Body = new Dictionary<string, string> {
                { "username", "client" },
                { "password", "client" }
            }
        };
        currentRequest.Headers["Accept"] = "application/json";
        currentRequest.Headers["Content-Type"] = "application/json";

        RestClient.Post(currentRequest)
        .Then(res => {
            Debug.Log(res.Text);
            //this.LogMessage("Success", JsonUtility.ToJson(res, true));
        })
        .Catch(err => Debug.Log(err.ToString()));
    }

    void TestNotification()
    {
        GameObject.Find("Canvas").GetComponent<PanelManager>().LaunchPopup("test test");
    }

    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError(pages[page] + ": HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                    break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadUserData()
    {
        SetName();
    }

    public void ClearUserData()
    {
        isUserLogged = false;
        GameObject.Find("UsernameTextUpper").GetComponent<Text>().text = "";
    }

    public void SetName()
    {
        GameObject.Find("UsernameTextUpper").GetComponent<Text>().text = username;
    }

    public void LogOutButton()
    {
        GameObject.Find("Canvas").GetComponent<PanelManager>().ShowAndHideOthers("SignIn");
        
        ClearUserData();
    }

    [System.Serializable]
    public struct TokenJson
    {
        public string access_token;
    }

    public IEnumerator SetToken()
    {
        //AuthManager.reference.username = GameObject.Find("InputFieldEmail").GetComponent<InputField>().text;
        //AuthManager.reference.password = GameObject.Find("InputFieldPassword").GetComponent<InputField>().text;
        Client http = new Client();
        Debug.Log(AuthManager.reference.username);
        Debug.Log(AuthManager.reference.password);

        LoginInfo info = new LoginInfo(AuthManager.reference.username, AuthManager.reference.password);
        Request request = new Request("http://15.188.80.3:8000/api/users/login")
            .Post(RequestBody.From<LoginInfo>(info));
        yield return http.Send(request);

        if (http.IsSuccessful())
        {
            Response resp = http.Response();
            Debug.Log("status: " + resp.Status().ToString() + "\nbody: " + resp.Body());
            TokenJson tokenData = JsonUtility.FromJson<TokenJson>(resp.Body());
            Debug.Log(tokenData.access_token);
            AuthManager.reference.token = tokenData.access_token;

            StartCoroutine(GetLogin());
        }
        else
        {
            Debug.Log("error: " + http.Error());
        }
    }

    [System.Serializable]
    public struct LoginJson
    {
        public string username;
        public string email;
        public string identifier;
        public int balance;
    }

    public IEnumerator GetLogin()
    {
        Client http = new Client();
        Request request = new Request("http://15.188.80.3:8000/api/users/me")
            .AddHeader("Authorization", "Bearer " + AuthManager.reference.token)
            .Get();
        yield return http.Send(request);

        if (http.IsSuccessful())
        {
            Response resp = http.Response();
            Debug.Log("status: " + resp.Status().ToString() + "\nbody: " + resp.Body());
            LoginJson loginData = JsonUtility.FromJson<LoginJson>(resp.Body());
            Debug.Log("LOGIN OK");
            Debug.Log(loginData.username);
            Debug.Log(loginData.identifier);
            Debug.Log(loginData.balance);
            AuthManager.reference.loginInfo = loginData;
        }
        else
        {
            Debug.Log("error: " + http.Error());
        }
    }
}
