using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZXing;
using ZXing.QrCode;
using UnityEngine.UI;

#if (UNITY_ANDROID)
using UnityEngine.Android;
#endif

public class QrRead : MonoBehaviour
{
    private WebCamTexture camTexture;
    private Rect screenRect;

    public float param1;
    public float param2;
    public float param3;
    public float param4;

    public bool codedObtained;
    public bool cameraOn;
    public string code;
    public Text codeText;
    public bool isCameraInit;
    public float timer;

    void Start()
    {
        isCameraInit = false;
        codedObtained = false;
        cameraOn = false;
        timer = 0.3f;

#if (UNITY_ANDROID)
        if (Permission.HasUserAuthorizedPermission(Permission.Microphone) || Permission.HasUserAuthorizedPermission(Permission.Camera))
        {

        }
        else
        {
            Permission.RequestUserPermission(Permission.Microphone);
            string[] permsissions = { Permission.Microphone, Permission.Camera };
            Permission.RequestUserPermissions(permsissions);
        }
#else
        initCamera();
#endif
    }

    void Update()
    {
        if (!isCameraInit)
        {
#if (UNITY_ANDROID)
        if (Permission.HasUserAuthorizedPermission(Permission.Microphone) || Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            initCamera();
        }
#endif
        }
    }

    void initCamera()
    {
        screenRect = new Rect(Screen.width * param1, Screen.height * param2, Screen.width * param3, Screen.height * param4);
        isCameraInit = true;
        WebCamDevice[] devices = WebCamTexture.devices;
        Debug.Log(devices.Length.ToString());
        codeText.text = devices.Length.ToString();
        screenRect = new Rect(Screen.width * param1, Screen.height * param2, Screen.width * param3, Screen.height * param4);
        camTexture = new WebCamTexture();
        camTexture.requestedHeight = Screen.height;
        camTexture.requestedWidth = Screen.width;
        if (camTexture != null)
        {
            camTexture.Play();
        }
    }

    void OnGUI()
    {
        if(cameraOn && !codedObtained)
        {
            ShowCamera();
        }
    }

    void ShowCamera()
    {
#if (UNITY_ANDROID)
        Matrix4x4 matrixBackup = GUI.matrix;
        Vector2 pivot = new Vector2(screenRect.xMin + screenRect.width * 0.5f, screenRect.yMin + screenRect.height * 0.5f);
        GUIUtility.RotateAroundPivot(90, pivot);

        // drawing the camera on screen
        GUI.DrawTexture(screenRect, camTexture, ScaleMode.ScaleToFit);
        // do the reading � you might want to attempt to read less often than you draw on the screen for performance sake

        GUI.matrix = matrixBackup;
#else
        GUI.DrawTexture(screenRect, camTexture, ScaleMode.ScaleToFit);
#endif
        timer -= Time.deltaTime;
        if(timer < 0f)
        {
            timer = 0.3f;
            TryReadQR();
        }
        
    }

    void TryReadQR()
    {
        try
        {
            IBarcodeReader barcodeReader = new BarcodeReader();
            // decode the current frame
            var result = barcodeReader.Decode(camTexture.GetPixels32(), camTexture.width, camTexture.height);
            if (result != null)
            {
                Debug.Log("DECODED TEXT FROM QR: " + result.Text);
                code = result.Text;
                codedObtained = true;
                codeText.text = code;
            }
        }
        catch (System.Exception ex) { Debug.LogWarning(ex.Message); }
    }

    public void SetCamOn()
    {
        this.cameraOn = true;
    }

    public void SetCamOff()
    {
        this.cameraOn = false;
    }
}
