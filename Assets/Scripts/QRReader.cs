using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZXing;
using ZXing.QrCode;
using UnityEngine.UI;

#if (UNITY_ANDROID)
using UnityEngine.Android;
#endif

public class QRReader : MonoBehaviour
{
    public ClientLogic clientLogic;

    private WebCamTexture camTexture;
    private Rect screenRect;

    public bool codedObtained;
    public bool cameraOn;
    public string code;
    public bool isCameraInit;
    public float timer;

    // Start is called before the first frame update
    void Start()
    {
        isCameraInit = false;
        codedObtained = false;
        cameraOn = false;
        timer = 0.3f;

#if (UNITY_ANDROID)
        if (Permission.HasUserAuthorizedPermission(Permission.Microphone) || Permission.HasUserAuthorizedPermission(Permission.Camera))
        {

        }
        else
        {
            Permission.RequestUserPermission(Permission.Microphone);
            string[] permsissions = { Permission.Microphone, Permission.Camera };
            Permission.RequestUserPermissions(permsissions);
        }
#endif
    }

    // Update is called once per frame
    void Update()
    {
#if (UNITY_ANDROID)
        if (!isCameraInit)
        {
            if (Permission.HasUserAuthorizedPermission(Permission.Microphone) || Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                initCamera();
            }
        }
#endif
    }

    void initCamera()
    {
        screenRect = new Rect(Screen.width * 0.16f, Screen.height * 0f, Screen.width * 0.71f, Screen.height * 0.62f);
        isCameraInit = true;
        WebCamDevice[] devices = WebCamTexture.devices;
        camTexture = new WebCamTexture();
        camTexture.requestedHeight = Screen.height;
        camTexture.requestedWidth = Screen.width;
        if (camTexture != null)
        {
            camTexture.Play();
        }
    }

    void OnGUI()
    {
        if (cameraOn && !codedObtained)
        {
            ShowCamera();
        }
    }

    void ShowCamera()
    {
#if (UNITY_ANDROID  && !UNITY_EDITOR)
        Matrix4x4 matrixBackup = GUI.matrix;
        Vector2 pivot = new Vector2(screenRect.xMin + screenRect.width * 0.5f, screenRect.yMin + screenRect.height * 0.5f);
        GUIUtility.RotateAroundPivot(90, pivot);

        // drawing the camera on screen
        GUI.DrawTexture(screenRect, camTexture, ScaleMode.ScaleToFit);
        // do the reading � you might want to attempt to read less often than you draw on the screen for performance sake

        GUI.matrix = matrixBackup;
#else
        GUI.DrawTexture(screenRect, camTexture, ScaleMode.ScaleToFit);
#endif
        timer -= Time.deltaTime;
        if (timer < 0f)
        {
            timer = 0.3f;
            TryReadQR();
        }

    }

    void TryReadQR()
    {
        try
        {
            Debug.Log("Trying detect QR...");
            IBarcodeReader barcodeReader = new BarcodeReader();
            // decode the current frame
            var result = barcodeReader.Decode(camTexture.GetPixels32(), camTexture.width, camTexture.height);
            if (result != null)
            {
                Debug.Log("DECODED TEXT FROM QR: " + result.Text);
                code = result.Text;
                codedObtained = true;
                clientLogic.ManageScannedCode(code);
            }
        }
        catch (System.Exception ex) { Debug.LogWarning(ex.Message); }
    }

    public void SetCamOn()
    {
        code = "";
        codedObtained = false;
        this.cameraOn = true;

        GameObject.Find("MainClient").GetComponent<ClientLogic>().ShowFinishButton();
    }

    public void SetCamOff()
    {
        this.cameraOn = false;
    }
}