using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Shop
{
    public char id;
    public string title;
    public string description;

    public Shop(char id, string title, string description)
    {
        this.id = id;
        this.title = title;
        this.description = description;
    }
}

public class ShopRow : MonoBehaviour
{
    public Text idText;
    public Text titleText;
    public Text descriptionText;

    public void InitRow(Shop shop)
    {
        idText.text = shop.id.ToString();
        titleText.text = shop.title;
        descriptionText.text = shop.description;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
