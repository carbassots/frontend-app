using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager : MonoBehaviour
{
    public GameObject panelsObject;
    Dictionary<string, GameObject> panels;
    public GameObject popupPrefab;

    private void Awake()
    {
        panels = new Dictionary<string, GameObject>();
        for(int i=0; i<panelsObject.transform.childCount; i++)
        {
            Transform currentChild = panelsObject.transform.GetChild(i);
            panels.Add(currentChild.name, currentChild.gameObject);
        }

        HideAll();

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HideAll()
    {
        foreach(GameObject panel in panels.Values)
        {
            panel.SetActive(false);
        }
    }

    public void Show(string panelName)
    {
        GameObject panel;
        panels.TryGetValue(panelName, out panel);
        panel.SetActive(true);
    }

    public void ShowAndHideOthers(string panelName)
    {
        HideAll();
        Show(panelName);
    }

    public void LaunchPopup(string message)
    {
        GameObject popup = Instantiate(popupPrefab);
        popup.transform.SetParent(this.transform);
        PopupLogic popupLogic = popup.GetComponent<PopupLogic>();
        popupLogic.Init(message);
    }
}
