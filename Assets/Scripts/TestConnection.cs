using SimpleHTTP;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class LoginInfo
{
    public string username;
    public string password;
    public LoginInfo(string username, string password)
    {
        this.username = username;
        this.password = password;
    }
}

public class TestConnection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Post());
    }
    IEnumerator Post() { 
        Client http = new Client();
        LoginInfo info = new LoginInfo("client", "client");
        Request request = new Request("http://15.188.80.3:8000/api/users/login")
            .Post(RequestBody.From<LoginInfo>(info));
        yield return http.Send(request);

        if (http.IsSuccessful())
        {
            Response resp = http.Response();
            Debug.Log("status: " + resp.Status().ToString() + "\nbody: " + resp.Body());
        }
        else
        {
            Debug.Log("error: " + http.Error());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
