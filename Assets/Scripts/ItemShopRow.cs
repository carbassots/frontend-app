using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[System.Serializable]
public class ItemShop
{
    public string title;
    public int cost;
    public string urlItem;

    public string formatCost()
    {
        int euros = cost / 100;
        int cents = cost % 100;
        return euros + "," + cents;
    }
}
public class ItemShopRow : MonoBehaviour
{
    public Text titleText;
    public Text costText;
    public Image image;

    IEnumerator DownloadImage(string MediaUrl)
    {
        Debug.Log("Loading ...." + MediaUrl);
        WWW www = new WWW(MediaUrl);   // create WWW object pointing to the url
        yield return www;         // start loading whatever in that url ( delay happens here )

        Debug.Log("Loaded");
        image.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
    }

    public void InitItem(ItemShop item)
    {
        titleText.text = item.title;

        costText.text = item.formatCost();
        StartCoroutine(DownloadImage(item.urlItem));
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
