using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;

public class ShopLogic : MonoBehaviour
{
    public Text numberOfCaragols;
    public string message = "test";
    Texture2D qrTexture;
    public bool isQRActive;
    public Button finishButton;

    public float param1;
    public float param2;
    public float param3;
    public float param4;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnEnable()
    {
        GameObject.Find("AuthManager").GetComponent<AuthManager>().LoadUserData();
    }

    // Shop send so client receive
    // Inverse text
    public void SendCaragols()
    {
        // API call
        message = "receive " + GameObject.Find("InputFieldCaragols").GetComponent<InputField>().text;
        GenerateQR();
    }

    public void ReceiveCaragols()
    {
        // API call
        message = "send " + GameObject.Find("InputFieldCaragols").GetComponent<InputField>().text;
        GenerateQR();
    }

    public void GenerateQR()
    {
        qrTexture = UpdateQR(message);
        isQRActive = true;
        finishButton.gameObject.SetActive(true);
    }

    private static Color32[] Encode(string textForEncoding, int width, int height)
    {
        var writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions
            {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }

    public Texture2D UpdateQR(string text)
    {
        var encoded = new Texture2D(256, 256);
        var color32 = Encode(text, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }

    void OnGUI()
    {
        if(isQRActive)
        {
            if (GUI.Button(new Rect(Screen.width * param1, Screen.height * param2, Screen.width * param3, Screen.height * param4), qrTexture, GUIStyle.none)) { }
        }
    }

    public void FinishButton()
    {
        isQRActive = false;
        finishButton.gameObject.SetActive(false);
    }
}
