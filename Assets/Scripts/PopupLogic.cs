using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupLogic : MonoBehaviour
{
    public GameObject descriptionGameObject;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Init(string description)
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        rectTransform.anchorMin = new Vector2(0.15f, 0.1f);
        rectTransform.anchorMax = new Vector2(0.85f, 0.9f);
        rectTransform.localScale = Vector3.one;

        descriptionGameObject.GetComponent<Text>().text = description;

        
        RectTransform descriptionRectTransform = GetComponent<RectTransform>();
        //descriptionRectTransform.anchorMin = new Vector2(0f, 0f);
        //descriptionRectTransform.anchorMax = new Vector2(1f, 1f);
        rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, 0);
        rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, 0);
        rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, 0);
        rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0, 0);

        rectTransform.anchorMin = new Vector2(0.15f, 0.1f);
        rectTransform.anchorMax = new Vector2(0.85f, 0.9f);
        rectTransform.localScale = Vector3.one;

    }

    public void Close()
    {
        Destroy(this.gameObject);
    }
}
