using SimpleHTTP;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignInLogic : MonoBehaviour
{
    public Button clientButton;
    public Button shopButton;
    public Button logOutButton;

    public Text nameText;
    public Text passwordText;
    public Text mailText;
    public string currentRole;

    // Start is called before the first frame update
    void Start()
    {
        currentRole = "client";
        logOutButton.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnEnable()
    {
        currentRole = "client";
        logOutButton.gameObject.SetActive(false);
    }

    public void Register()
    {
        AuthManager authManager = GameObject.Find("AuthManager").GetComponent<AuthManager>();
        authManager.username = nameText.text;
        authManager.role = currentRole;
        // Send to API
        /*
        nameText.text;
        mailText.text;
        passwordText.text;
        currentRole;
        */

        GoToMainScreen();
    }

    public void SignIn()
    {
        AuthManager authManager = GameObject.Find("AuthManager").GetComponent<AuthManager>();
        authManager.username = nameText.text;
        authManager.role = currentRole;
        authManager.password = passwordText.text;


        GoToMainScreen();
    }

    public void GoToMainScreen()
    {
        logOutButton.gameObject.SetActive(true);
        AuthManager authManager = GameObject.Find("AuthManager").GetComponent<AuthManager>();
        string nextScreen;

        if (authManager.role == "client")
        {
            nextScreen = "MainClient";
        }
        else
        {
            nextScreen = "MainShop";
        }

        GameObject.Find("Canvas").GetComponent<PanelManager>().ShowAndHideOthers(nextScreen);
    }

    public void ClientButtonPress()
    {
        clientButton.GetComponent<Image>().color = new Color(255, 255, 255);
        shopButton.GetComponent<Image>().color = new Color(181, 181, 181);
        currentRole = "client";
    }

    public void ShopButtonPress()
    {
        shopButton.GetComponent<Image>().color = new Color(255, 255, 255);
        clientButton.GetComponent<Image>().color = new Color(181, 181, 181);
        currentRole = "shop";
    }
}
