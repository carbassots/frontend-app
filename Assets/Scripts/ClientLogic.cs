using Models;
using Proyecto26;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClientLogic : MonoBehaviour
{
    public Text caragolsText;
    public Button finishButton;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnEnable()
    {
        StartCoroutine(AuthManager.reference.SetToken());

        GameObject.Find("AuthManager").GetComponent<AuthManager>().LoadUserData();
    }

    public void ManageScannedCode(string code)
    {
        // Download info
        //id preu, botiga, treballador, comprador, tipus de producte, acceptat

        Debug.Log("Received code: " + code);
        if (code.Contains("receive"))
        {
            string[] texts = code.Split();
            CaragolsReceived(float.Parse(texts[1]));

        } else if(code.Contains("send"))
        {
            string[] texts = code.Split();
            CaragolsSpent(float.Parse(texts[1]));
        }

        finishButton.gameObject.SetActive(false);
    }

    public void CaragolsReceived(float newCaragols)
    {
        AuthManager.reference.caragols += newCaragols;
        // Download new data
        string message = "You have received " + newCaragols + " caragols. You have now " + AuthManager.reference.caragols;
        GameObject.Find("Canvas").GetComponent<PanelManager>().LaunchPopup(message);
    }

    public void CaragolsSpent(float spentCaragols)
    {
        AuthManager.reference.caragols -= spentCaragols;
        // Download new data
        string message = "You have spent " + spentCaragols + " caragols. You have now " + AuthManager.reference.caragols;
        
        GameObject.Find("Canvas").GetComponent<PanelManager>().LaunchPopup(message);
    }

    public void RefreshUserData()
    {
        // Download data
        caragolsText.text = AuthManager.reference.caragols.ToString();
    }

    public void FinishButton()
    {
        GameObject.Find("QRReader").GetComponent<QRReader>().SetCamOff();
        finishButton.gameObject.SetActive(false);
    }

    public void ShowFinishButton()
    {
        finishButton.gameObject.SetActive(true);
    }
}
