using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoogleAPIKeyReader : MonoBehaviour
{
    public static string APIKEY;

    void Awake()
    {
        string path = Application.dataPath + "/GOOGLEKEY.txt";
        APIKEY = System.IO.File.ReadAllText(path).Trim();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
