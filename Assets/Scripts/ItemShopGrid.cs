using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemShopGrid : MonoBehaviour
{
    public ItemShop[] items;
    public GameObject prefabItem;

    void InitItems()
    {
        for(int i=0; i < items.Length; i++)
        {
            GameObject row = Object.Instantiate(prefabItem, this.transform);
            ItemShopRow shoprow = row.GetComponent<ItemShopRow>();
            Debug.Log(row.ToString());
            shoprow.InitItem(items[i]);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        InitItems();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
