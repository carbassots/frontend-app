using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopList : MonoBehaviour
{
    public Shop[] shops;

    public GameObject prefabShopRow;
    void InitRows()
    {
        ScrollRect sct = this.GetComponent<ScrollRect>();
        foreach (Shop shop in shops)
        {
            GameObject row = Object.Instantiate(prefabShopRow, sct.content.transform);
            ShopRow shoprow = row.GetComponent<ShopRow>();
            Debug.Log(row.ToString());
            shoprow.InitRow(shop);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        InitRows();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
